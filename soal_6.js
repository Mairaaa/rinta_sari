// Bilangan Fibonacci

function bilFibonacci(n) {
    let angkaSebelum = 0;
    let angkaSesudah = 1;
    let total;
    if (n === 0) {
      return angkaSebelum;
    }
    while (n > 1) {
      total = angkaSebelum + angkaSesudah;
      angkaSebelum = angkaSesudah;
      angkaSesudah = total;
      n--;
    }
    return angkaSesudah;
  }

console.log(bilFibonacci(3)); 


  