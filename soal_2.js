function prima(n) {             //fungsi prima mengambil nomor "n" sebagai input
    if (n < 2){                 // jika "n" kurang dari 2 maka hasilnya false karena bukan bil prima
        return false;
    }

for (let i = 2; i < n; i++) {   // perulangan i dimulai dari 2, i kurang dari "n" dan i tambah satu 

    if (n % i === 0) {          // jika "n" modulus i sama dengan 0 return nya false, === 
        return false;           
    }
}

return true;                    // return true jika kondisi diatas tidak terpenuhi
}

// memanggil fungsi
console.log(prima(5));           
console.log(prima(12));

1 === '1' 


