// Membuat fungsi untuk menjumlahkan array yang tergolong ke dalam bilangan prima saja
function bilPrima(arr) {
    let sum = 0;                              // deklarasi sum, nilai yg diberikan 0
    for (let i = 0; i < arr.length; i++) {
      if (prima(arr[i])) {
        sum += arr[i];
      }
    }
    return sum;
  }
  
  // Membuat fungsi untuk mengecek bilangan prima pada array
  function prima(n) {
    if (n <= 1) {
      return false;
    }
    for (let i = 2; i <= Math.sqrt(n); i++) {
      if (n % i === 0) {
        return false;
      }
    }
    return true;
  }
  
  console.log(bilPrima([5, 6, 7, 8, 9, 10, 11]));
